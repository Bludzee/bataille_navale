/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vilcoque.matheo
 */
public class Bateau {

    private int X;
    private int Y;
    public int MIN_X = 0;
    public int MIN_Y = 0;
    private int drapeau;
    private boolean detruit;

    public Bateau(int uneCoorX, int uneCoorY, int unDrapeau, boolean unBateauDetruit) {
        this.X = uneCoorX;
        this.Y = uneCoorY;
        this.drapeau = unDrapeau;
        this.detruit = unBateauDetruit;
    }

    public Bateau(int uneCoorX, int uneCoorY, int unDrapeau) {
        this.X = uneCoorX;
        this.Y = uneCoorY;
        this.drapeau = unDrapeau;
        this.detruit = false;

    }

    /**
     * @return
     */
    public int getCoorX() {
        return X;
    }

    /**
     * @return
     */
    public double getCoorY() {
        return Y;
    }

    /**
     * @return
     */
    public int getDrapeau() {
        return drapeau;
    }

    /**
     * @return
     */
    public boolean estDetruit() {
        if (this.detruit == true) {
            return detruit = true;
        } else {
            return detruit = false;
        }
    }

    public double Distance(Bateau unBateau) {
        return Math.sqrt(Math.pow(X - unBateau.getCoorX(), 2) + Math.pow(Y - unBateau.getCoorY(), 2));
    }

    public void avance(int unitsX, int UnitsY){
        
    }
    
    public void coule() {

    }
}
